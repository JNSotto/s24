db.inventory.insertMany([
	{
		name: "Captain America Shield",
		price: 50000,
		qty: 17,
		company: "Hydra and Co"
	},
	{
		name: "Mjolnir",
		price: 75000,
		qty: 24,
		company: "Asgard Production"
	},
	{
		name: "Iron Man Suit",
		price: 25400,
		qty: 25,
		company: "Stark Industries"
	},
	{
		name: "Eye of Agamotto",
		price: 28000,
		qty: 51,
		company: "Sanctum Company"
	},
	{
		name: "Iron Spider Suit",
		price: 30000,
		qty: 24,
		company: "Stark Industries"
	}
])

// Query Operators
	// allows us to be more flexible when querying in MongoDB, we can opt to find, update and delete documents based on some condition instead of just specific criterias.

// Comparison Query Operators
	// $gt and $gte
	// Syntax:db.collections.find({field: {$gt: value}}) and db.collections.find({field: {$gte: value}})

	// $gt- greater than, allows us to find values that are greater than the given value.
	// $gte- greater than or equal, allows us to find values that are greater than or equal the given value.

	db.inventory.find(
		
		{
			price: {$gte: 75000}
		}

	)

	// $lt and $lte
	// Syntax: db.collections.find({field: {$lt: value}}) and db.collections.find({field: {$lte: value}})

	// $lt- less than, allows us to find values that are less than the given value.
	// $lte- less than or equal, allows us to find values that are less than or equal the given value.

	db.inventory.find(
		
		{
			qty: {$lt: 20}
		}
	
	)

	// $ne
	// Syntax: db.collections.find({field: {$ne: value}})

	// $ne- not equal, returns a document that values are not equal to the given value.

	db.inventory.find(
		
		{
			qty: {$ne: 10}
		}

	)

	// $in
	// Syntax: db.collections.find({field: {$in: value}})

	//$in- allows us to find documents that satisfy either of the specified values.

	db.inventory.find(
		{
			price: {$in: [25400, 30000]}
		}

	)

	// REMINDER: Although, you can express this query using $or operator, choose the $in operator rather than $or operator when performing equality checks on the same field.


	/*
		Mini-Activity
			1. In the inventory collection, return all documents that have the price greater than or equal to 50000.
			2. In the inventory collection, return all documents that have the quantity of 24 and 16.
	*/

	// Solution for #1
	db.inventory.find(
		{
			price: {$gte: 50000}
		}
	)

	// Solution for #2
	db.inventory.find(
		{
			qty: {$in: [24, 16]}
		}
	)

	// UPDATE and DELETE
		// 2 arguments namely: query criteria, update

		db.inventory.updateMany(

			{
				qty: {$lte: 24}
			}, 
			{
				$set: {isActive: true}
			}

		)

	/*
		Mini-Activity
			In the inventory collection, update the quantity to 17 of the product with a price less than 28000.

	*/

	// Solution
	db.inventory.updateMany(

		{
			price: {$lt: 28000}
		}, 
		{
			$set: {qty: 17}
		}

	)


// Logical Operators
	// $and
	// Syntax:db.collections.find({$and: [{criteria1}, {criteria2}]})

	// $and- allows us to return document/s that satisfies all given conditions.

	db.inventory.find({$and: 
		[
			{
				price: {$gte: 50000}
			}, 
			{
				qty: 17
			}
		]
	})

	// $or
	// Syntax:db.collections.find({$or: [{criteria1}, {criteria2}]})

	// $or- allows us to return document/s that satisfies either of the given conditions.

	db.inventory.find({$or: 
		[
			{
				qty: {$lt: 24}
			}, 
			{
				isActive: false
			}
		]
	}) 


	/*
		Mini-Activity
		In the inventory collection, find a product with a quantity less than or equal to 24 or has a price less than or equal to 30000

		Make it an active product.

	*/

		// Solution:
		db.inventory.updateMany(
			{
				$or: 
				[
					{qty: {$lte: 24}}, 
					{price: {$lte: 30000}}
				]
			}, 
			{
				$set: 
					{isActive: true}
			}

			)

// Evaluation Query Operator
	// $regex
		// Syntax
		{field: {$regex: /pattern/}}
			// case sensitive query
		{field: {$regex: /pattern/, $options: '$optionValue'}}
			// case insensitive query

		db.inventory.find(
			{
				name: {$regex: 'S'}
			}
		)

		db.inventory.find(
        {
                name: {$regex: 'A', $options: '$i'}
        }
	)


	/*
		Mini-Activity
			In the inventory collection, return document/s that have a name containing a letter 'i' and a price greater than 70000.

	*/

	// Solution:
	db.inventory.find(
			{
				$and:
					[
						{
							name: {$regex: 'i', $options: '$i'}
						},
						{
							price: {$gt: 70000}
						}

					]
			}


	)

// Field Projection
	// allows us to hide/show properties of a returned document/s after a query. When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.

	// Inclusion and Exclusion
		// Syntax: 
			db.collections.find({criteria}, {field: 1})
				// field: 1- allows us to include/add specific fields only when retrieving documents. The value provided is 1 to denote that the field is being included.

				db.inventory.find({}, {name: 1})

				db.inventory.find({}, {_id: 0, name:1, price:1})

		// Syntax: 
			db.collections.find({criteria}, {field: 0})
				// field: 0- allows us to exclude the specific field.

				db.inventory.find({}, {qty: 0})

				db.inventory.find({}, {_id: 0, name:0, price:0})

			// REMINDER: When using field projection, field inclusion and exclusion may not be used in the same time. Excluding the "_id" field.

			// projection with criteria
			db.inventory.find(
				{
					company: {$regex: 'A'}
				}, 
				{
					name: 1, company:1, _id:0
				}

			)

		/*
			Mini-Activity:
				In the inventory collection, return all documents that have a name containing an uppercase letter 'A' and a price less than or equal to 30000.

				Show only the name and the price of the product.

				Hide the id field.
	
		*/

		// Solution:
			db.inventory.find(
			{
				$and: 
				[
					{
						name: {$regex: 'A'}
					},
					{
						price: {$lte: 30000}
					}

				]
			},
				{
					name: 1, price: 1, _id: 0
				}

		)


