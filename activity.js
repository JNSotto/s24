db.users.insertMany([
{
	firstName:"Nikola",
	lastName:"Tesla",
	age:64,
	department:"Communication"
},	
{
	firstName:"Marie",
	lastName:"Curie",
	age:28,
	department:"Media and Arts"
},	
{
	firstName:"Charles",
	lastName:"Darwin",
	age:51,
	department:"Human Resources"
},	
{
	firstName:"Rene",
	lastName:"Descartes",
	age:50,
	department:"Media and Arts"
},	
{
	firstName:"Albert",
	lastName:"Eintein",
	age:51,
	department:"Human Resources"
},	
{
	firstName:"Micheal",
	lastName:"Faraday",
	age:30,
	department:"Communication"
},
])



db.users.find(
{
	$or: 
	[
	{
		firstName: {$regex: 's'}
	},
	{
		lastName: {$regex: 'd'}
	}

	]
},
{
	firstName: 1, lastName: 1, _id: 0
}

)


db.users.find(
{
	$and: 
	[
	{
		department:"Human Resources"
	},
	{
		age: {$gte: 50}
	}

	]
}

)

db.users.find(
{
	$and: 
	[
	{
		firstName: {$regex: 'e', $options:'$i'}
	},
	{
		age: {$lte: 30}
	}

	]
}

)